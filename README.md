# Calendar 8.x-2.x

## INTRODUCTION
Calendar for D8 v2 is a near complete rewrite of the Calendar module.
This module provides views plugins for style, argument, pager and (header) area.

Relevant changes from 8.x-1.x branch:

1. No row plugin
The row plugin has been abandoned. Use Content or Fields from core Views.
2. Flex based templates
Since css flex is now supported by all major browsers the rendering of the
calendars is based on flex positioning.
Also the css is now generated from sass files. The templates strive to make
calendars less complicated to assist in rendering on different devices
 ("mobile first") and easier customization.
3. Removed logic from template layer.
The 1.x version contains significant logic in the included theme file. Most of
the logic is concentrated in the Style plugin.
The Style plugin still uses CalendarEvent, CalendarHelper and CalendarStyleInfo.
4. Use DrupalDateTime
Where possible (or at least so it seemed) custom date arithmetic is replaced
with methods on DrupalDateTime (or in some cases DateTime).
5. Use custom arguments for all granularities
This version uses its own views arguments for all granularities for the
following reasons:

    * it makes it easier to discern between other (date) arguments and calendar date
    arguments
    * the arguments contain functionality to show calendar events that start and/or
    end outside the current date range
    (e.g. a very long event that is started before the current week is still shown).

6. Inject CalendarHelper and CalendarStyleInfo as dependency
Where feasible these classes are added through DI to plugins, e.g. style
, argument, header and pager plugins.

### Warning ###
The 8.x-.2.x is NOT feature complete compared to the 8.x-1.x or 7.x-3.5 branch.
There are no guarantees it will ever become that.

## REQUIREMENTS
The module obviously depends on Views. To be of any use a view should be based
on some entity having a date and/or daterange field.

## INSTALLATION
Install this module as usual, no external libraries required.

## CONFIGURATION
This module has no site wide config options.

## USAGE
Add a new view with any content entity as its base. Select the Calendar style
plugin, and select Settings to provide configuration options, the most
important one being 'granularity': year, month, week or day.
Under Advanced add a Calendar Date as a contextual filter. Select an argument
based on the granularity for the display. You may combine this with other
 arguments (e.g. content type, user id etc).

## TODOS
There's still a lot of work to be done... These probably fall into the following
 categories:

    * write unit tests
    * end user tests and bug fixing
    * make the 2.x branch feature complete
    * refactor architectural patterns, e.g. the division between
     CalendarHelper and CalendarStyleInfo
    * better responsive (mobile) templates

