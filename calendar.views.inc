<?php

/**
 * @file
 * Provides views data for the calendar module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_views_data_alter().
 */
function calendar_views_data_alter(array &$data) {
  $data['views']['calendar_header'] = [
    'title' => t('Calendar Header'),
    'help' => t('Calendar heading or pager.'),
    'area' => [
      'id' => 'calendar_header',
    ],
    'theme' => 'calendar_header',
  ];
}

/**
 * Implements hook_field_views_data_alter().
 *
 * @TODO is this still used after adding custom argument plugins?
 */
function calendar_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {
  foreach ($data as $table_name => $table_data) {

    // Set the 'datetime' filter type.
    if (isset($data[$table_name][$field_storage->getName() . '_value'])) {

      if ($data[$table_name][$field_storage->getName() . '_value']['filter']['id'] == 'datetime') {

        // Create granularity arguments.
        $group = $data[$table_name][$field_storage->getName() . '_value']['group'];
        $arguments = [
          'year_week' => t('Calendar Date in the form of Y + W.'),
          'year_month' => t('Calendar Date in the form of Y + m.'),
          'day' => t('Calendar Date in the form of Y + m + d.'),
        ];
        foreach ($arguments as $argument_type => $help_text) {
          $data[$table_name][$field_storage->getName() . '_value_' . $argument_type] = [
            'title' => 'Calendar ' . $field_storage->getLabel() . ' (' . $argument_type . ')',
            'help' => $help_text,
            'argument' => [
              'field' => $field_storage->getName() . '_value',
              'id' => 'calendar_' . $argument_type,
              'entity_type' => $field_storage->getTargetEntityTypeId(),
              'field_name' => $field_storage->getName(),
            ],
            'group' => $group,
          ];
        }
      }
    }
  }
}
