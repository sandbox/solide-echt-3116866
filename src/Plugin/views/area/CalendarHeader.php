<?php

namespace Drupal\calendar\Plugin\views\area;

use Drupal\calendar\CalendarHelper;
use Drupal\calendar\Plugin\views\argument\CalendarYearWeekDate;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area Calendar Header area.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("calendar_header")
 */
class CalendarHeader extends TokenizeAreaPluginBase implements ContainerFactoryPluginInterface {

  /**
   * DI CalendarHelper class.
   *
   * @var \Drupal\Calendar\CalendarHelper
   */
  protected $calendarHelper;

  /**
   * Extends the Date instance with CalendarHelper.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Calendar\CalendarHelper $calendarHelper
   *   Calendar helper service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $calendarHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->calendarHelper = $calendarHelper;
  }

  /**
   * Create function for Date query
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @return static
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('calendar.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // Override defaults to from parent.
    $options['tokenize']['default'] = TRUE;
    $options['empty']['default'] = TRUE;
    // Provide our own defaults.
    $options['content'] = ['default' => ''];
    $options['pager_embed'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['content'] = [
      '#title' => $this->t('Heading'),
      '#type' => 'textfield',
      '#default_value' => $this->options['content'],
    ];
    $form['pager_embed'] = [
      '#title' => $this->t('Use Pager'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['pager_embed'],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {

    if (!$empty || !empty($this->options['empty'])) {
      $header_text = $date_argument = NULL;
      $render = [];
      $calendar_arguments = $this->calendarHelper->getCalendarArguments($this->view);
      foreach ($calendar_arguments as $date_argument) {

        switch ($date_argument['id']) {
          case 'calendar_day':
            $date = new DrupalDateTime($date_argument['argument']);
            $argument_text = $date->format('F d Y');
            $header_text = $this->renderTextField($argument_text);
            break;

          case 'calendar_year_week':
            $date = $this->calendarHelper->weekInfo($date_argument['argument']);
            $argument_text = t('Week') . ' ' . $date['weekno'] . ' ' . t('starting') . ' ' . $date['startweekdate']->format('F d Y');
            $header_text = $this->renderTextField($argument_text);
            break;

          case 'calendar_year_month':
            $date = new DrupalDateTime($date_argument['argument'] . '01');
            $argument_text = $date->format('F') . ' ' . $date->format('Y');
            $header_text = $this->renderTextField($argument_text);
            break;

          case 'calendar_year':
            break;

        }

        if (!$this->options['pager_embed']) {
          $render = [
            '#theme' => 'calendar_header',
            '#title' => $header_text,
          ];
        }
        else {
          if ($this->view->display_handler->renderPager()) {
            $exposed_input = isset($this->view->exposed_raw_input) ? $this->view->exposed_raw_input : NULL;
            $render = $this->view->renderPager($exposed_input);
            $render['#exclude'] = FALSE;
            $render['#items']['current'] = $header_text;
          }
        }
      }
      return $render;
    }

    return [];
  }

  /**
   * Render a text area with \Drupal\Component\Utility\Xss::filterAdmin().
   */
  public function renderTextField($value) {
    if ($value) {
      return $this->sanitizeValue($this->tokenizeValue($value), 'xss_admin');
    }
    return '';
  }

}
