<?php

namespace Drupal\calendar\Plugin\views\pager;

use Drupal\calendar\CalendarHelper;
use Drupal\calendar\Plugin\views\area\CalendarHeader;
use Drupal\calendar\Plugin\views\argument\CalendarYearMonthDate;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\pager\PagerPluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\datetime\Plugin\views\argument\Date;

/**
 * The plugin to handle calendar pager.
 *
 * @ingroup views_pager_plugins
 *
 * @ViewsPager(
 *   id = "calendar",
 *   title = @Translation("Calendar Pager"),
 *   short_title = @Translation("Calendar"),
 *   help = @Translation("Calendar Pager"),
 *   theme = "calendar_pager",
 *   register_theme = FALSE
 * )
 */
class CalendarPager extends PagerPluginBase implements ContainerFactoryPluginInterface {

  const NEXT = '+';
  const PREVIOUS = '-';

  /**
   * Calendar argument.
   *
   * @var string
   */
  protected $argument;

  /**
   * The style info for this calendar.
   *
   * @var \Drupal\Calendar\CalendarStyleInfo
   *   The calendar style info object.
   */
  protected $styleInfo;

  /**
   * DI CalendarHelper class.
   *
   * @var \Drupal\Calendar\CalendarHelper
   */
  protected $calendarHelper;

  /**
   * Extends the Date instance with CalendarHelper.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Calendar\CalendarHelper $calendarHelper
   *   Calendar helper service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $calendarHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->calendarHelper = $calendarHelper;
  }

  /**
   * Create function for Date query
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @return static
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('calendar.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->styleInfo = &$this->view->styleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function render($input) {
    $calendar_arguments = CalendarHelper::getCalendarArguments($this->view);
    foreach ($calendar_arguments as $date_argument) {
      if ($date_argument['argument']) {
        $items['previous'] = [
          'url' => $this->getPagerUrl($date_argument, self::PREVIOUS, $input),
        ];
        $items['next'] = [
          'url' => $this->getPagerUrl($date_argument, self::NEXT, $input),
        ];
      }
    }
    return [
      '#theme' => $this->themeFunctions(),
      '#items' => $items,
      '#exclude' => $this->options['exclude_display'],
    ];
  }

  /**
   * Get the href value for the pager link.
   *
   * @param string $date_argument
   *   The string used as the contextual filter.
   * @param string $mode
   *   Either '-' or '+' to determine which direction.
   * @param array $input
   *   Any extra GET parameters that should be retained, such as exposed
   *   input.
   * @TODO fix this
   *
   * @throws \Exception
   *
   * @return string
   *   An url containing a link.
   */
  protected function getPagerUrl($date_argument, $mode, array $input) {
    // @TODO add format to $date_argument array?
    $format = reset($this->view->argument)->getArgFormat();
    if ($format == 'YW') {
      $new = new \DateTime();
      $year = (int)substr($date_argument['argument'], 0, 4);
      $month = (int)substr($date_argument['argument'], 4, 2);
      $new->setISODate($year, $month);
    }
    else {
      $new = \DateTime::createFromFormat($format, $date_argument['argument']);
    }
    $new->modify($mode . '1 ' . $this->styleInfo->getGranularity());
    $value = $new->format($format);
    $current_position = 0;
    $arg_vals = [];
    /**
     * @var \Drupal\views\Plugin\views\argument\ArgumentPluginBase $handler
     */
    foreach ($this->view->argument as $name => $argument) {
      if (!$argument instanceof Date) {
        $arg_vals["arg_$current_position"] = $argument->getValue();
      }
      else {
        $arg_vals["arg_$current_position"] = $value;
      }
      $current_position++;
    }

    return $this->view->getUrl($arg_vals, $this->view->current_display);

  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['exclude_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude from Display'),
      '#default_value' => $this->options['exclude_display'],
      '#description' => $this->t('Use this option if you only want to display the pager in Calendar Header area.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['exclude_display'] = ['default' => FALSE];

    return $options;
  }

}
