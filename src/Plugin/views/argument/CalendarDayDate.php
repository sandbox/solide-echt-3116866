<?php

namespace Drupal\calendar\Plugin\views\argument;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\datetime\Plugin\views\argument\Date;
use Drupal\Calendar\CalendarHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for a day.
 *
 * @ViewsArgument("calendar_day")
 */
class CalendarDayDate extends Date {

  /**
   * Argument format.
   *
   * @var string
   */
  protected $argFormat = 'Ymd';

  /**
   * Inject CalendarHelper class.
   *
   * @var \Drupal\Calendar\CalendarHelper
   */
  protected $calendarHelper;

  /**
   * Extends the Date instance with CalendarHelper.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter
   * @param \Drupal\Calendar\CalendarHelper $calendarHelper
   *   Calendar helper service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $route_match,
    $date_formatter,
    $calendarHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_match, $date_formatter);
    $this->routeMatch = $route_match;
    $this->dateFormatter = $date_formatter;
    $this->calendarHelper = $calendarHelper;
  }

  /**
   * Create function for Date query
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param RouteMatchInterface $route_match
   *   The plugin.
   * @param DateFormatterInterface $date_formatter
   *   The plugin implementation definition.
   * @return static
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('calendar.helper')
    );
  }

  /**
   * Getter for the argFormat.
   *
   * @return string
   *   The argFormat.
   */
  public function getArgFormat() {
    return $this->argFormat;
  }

  /**
   * Build the query based upon the formula.
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();
    // Extend query logic only if field is of type daterange.
    if ($this->getFieldDefinition()->getType() === 'daterange') {

      if (!isset($this->argument)) {
        return;
      }
      else {
        $validated_argument = $this->calendarHelper->getCalendarArguments($this->view);
      }
      $this->argument = $validated_argument[0]['argument'];

      $date = new DrupalDateTime($this->argument);
      $date->format('Y-m-d H:i:s');

      // We need dates that begin before this day or end after this day too.
      // To build the expression get day before and after.
      $date_after = new DrupalDateTime($this->argument);
      $date_after->add(new \DateInterval('P1D'));
      $after = $date_after->format('Y-m-d H:i:s');

      $start_field_name = "$this->tableAlias.$this->realField";
      $end_field_name = substr($start_field_name, 0, -6) . '_end_value';
      $date_start = $this->query->getDateFormat($this->query->getDateField($start_field_name, TRUE), 'Y-m-d H:i:s', FALSE);
      $date_end = $this->query->getDateFormat($this->query->getDateField($end_field_name, TRUE), 'Y-m-d H:i:s', FALSE);

      $this->query->setWhereGroup('AND', 'StartDate');
      $expression1 = "$date_start < '$after'";
      $expression2 = "$date_end > '$date'";
      $this->query->addWhereExpression('StartDate', $expression1);
      $this->query->addWhereExpression('StartDate', $expression2);
    }
  }

}
