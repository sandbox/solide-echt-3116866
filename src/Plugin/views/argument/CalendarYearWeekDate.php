<?php

namespace Drupal\calendar\Plugin\views\argument;

use Drupal\calendar\CalendarHelper;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\datetime\Plugin\views\argument\Date;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for a day.
 *
 * @ViewsArgument("calendar_year_week")
 */
class CalendarYearWeekDate extends Date {

  /**
   * Argument format.
   *
   * @var string
   */
  protected $argFormat = 'YW';

  /**
   * DI CalendarHelper class.
   *
   * @var \Drupal\Calendar\CalendarHelper
   */
  protected $calendarHelper;

  /**
   * Extends the Date instance with CalendarHelper.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter
   * @param \Drupal\Calendar\CalendarHelper $calendarHelper
   *   Calendar helper service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $route_match,
    $date_formatter,
    $calendarHelper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_match, $date_formatter);
    $this->routeMatch = $route_match;
    $this->dateFormatter = $date_formatter;
    $this->calendarHelper = $calendarHelper;
  }

  /**
   * Create function for Date query
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param RouteMatchInterface $route_match
   *   The plugin.
   * @param DateFormatterInterface $date_formatter
   *   The plugin implementation definition.
   * @return static
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('date.formatter'),
      $container->get('calendar.helper')
    );
  }

  /**
   * Getter for the argFormat.
   *
   * @return string
   *   The argFormat.
   */
  public function getArgFormat() {
    return $this->argFormat;
  }

  /**
   * Build the query based upon the formula.
   *
   * Extend query logic only if field is of type daterange.
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    if ($this->getFieldDefinition()->getType() === 'daterange') {

      if (!isset($this->argument)) {
        return;
      }
      else {
        $validated_argument = $this->calendarHelper->getCalendarArguments($this->view);
      }
      $this->argument = $validated_argument[0]['argument'];

      $calendar_arguments = $this->calendarHelper->getCalendarArguments($this->view);

      foreach ($calendar_arguments as $date_argument) {
        $week_start = $this->calendarHelper->weekInfo($date_argument['argument']);
        $date_week_start = $week_start['startweekdate'];
        $date_week_end = $week_start['endweekdate'];
        $start_field_name = "$this->tableAlias.$this->realField";
        $end_field_name = substr($start_field_name, 0, -6) . '_end_value';
        $date_start = $this->query->getDateFormat($this->query->getDateField($start_field_name, TRUE),
          'Y-m-d H:i:s', FALSE);
        $date_end = $this->query->getDateFormat($this->query->getDateField($end_field_name, TRUE),
          'Y-m-d H:i:s', FALSE);
        $this->query->setWhereGroup('OR', 'StartDate');
        $this->query->setWhereGroup('OR', 'EndDate');
        $expression1 = "$date_start <= '$date_week_end'";
        $expression2 = "$date_end >= '$date_week_start'";
        $this->query->addWhereExpression('StartDate', $expression1);
        $this->query->addWhereExpression('EndDate', $expression2);

      }
    }
  }

}
