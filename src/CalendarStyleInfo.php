<?php

namespace Drupal\calendar;

/**
 * Defines a calendar style info object.
 *
 * @TODO sanitize obsolete setters and getters
 */
class CalendarStyleInfo {

  /**
   * Defines whether or not to show this calendar in a popup.
   *
   * @var bool
   *   TRUE to show the calendar in a popup, FALSE otherwise.
   */
  protected $calendarPopup;

  /**
   * Defines field for optional colors.
   *
   * @var string
   *   The field containing color info.
   */
  protected $colorField;

  /**
   * Defines a custom group by field.
   *
   * @var string
   *   A field to group items by.
   */
  protected $customGroupByField;

  /**
   * Defines a custom way of grouping by times.
   *
   * @var string
   *   The grouping by times.
   */
  protected $customGroupByTimes;

  /**
   * Defines day link setting.
   *
   * @var string
   *   The day link setting.
   */
  protected $dayLink;

  /**
   * The size of the calendar name.
   *
   * @var int
   *   The size of the calendar name.
   */
  protected $dayNameSize;

  /**
   * Defines the granularity for this view.
   *
   * @var string
   */
  protected $granularity;

  /**
   * Defines the way of grouping items.
   *
   * @var string
   *   The way of grouping items (e.g. 'hour', 'half').
   */
  protected $groupByTimes;

  /**
   * The maximum amount of items to show.
   *
   * @var int
   *   The maximum amount.
   */
  protected $maxItems;

  /**
   * Defines what the maximum items style is.
   *
   * @var string
   *   The maximum items style (e.g. 'hide').
   */
  protected $maxItemsStyle;

  /**
   * Defines multi- & allday style setting.
   *
   * @var string
   *   The multi- & allday style setting.
   */
  protected $multiAllDayStyle;

  /**
   * Defines multi- & allday title setting.
   *
   * @var string
   *   The multi- & allday title setting.
   */
  protected $multiAllDayTitle;

  /**
   * Defines multi- & allday view mode setting.
   *
   * @var string
   *   The multi- & allday view mode setting.
   */
  protected $multiAllDayViewMode;

  /**
   * Defines what the multi day theme is.
   *
   * @var int
   *   The index number of the multiday theme.
   */
  protected $multiDayTheme;

  /**
   * Defines whether or not this is a mini calendar.
   *
   * @var bool
   *   TRUE if the calendar is shown in mini, FALSE otherwise.
   */
  protected $mini;

  /**
   * Defines whether or not to display the week numbers.
   *
   * @var bool
   *   TRUE to display the week numbers, FAlSE otherwise.
   */
  protected $showWeekNumbers;

  /**
   * Defines what the theme style is.
   *
   * @var int
   *   The index number of the theme style.
   */
  protected $themeStyle;

  /**
   * Defines week link setting.
   *
   * @var string
   *   The week link setting.
   */
  protected $weekLink;

  /**
   * Setter for the calendar popup variable.
   *
   * @param bool $calendarPopup
   *   TRUE to show the calendar in a popup, FALSE otherwise.
   */
  public function setCalendarPopup($calendarPopup) {
    $this->calendarPopup = $calendarPopup;
  }

  /**
   * Getter for the calendar popup variable.
   *
   * @return bool
   *   TRUE to show the calendar in a popup, FALSE otherwise.
   */
  public function getCalendarPopup() {
    return $this->calendarPopup;
  }

  /**
   * Getter for the color field.
   *
   * @return string
   *   The field containing color info.
   */
  public function getColorField() {
    return $this->colorField;
  }

  /**
   * Setter for the color field.
   *
   * @param string $colorfield
   *   The field containing color info.
   */
  public function setColorField($colorfield) {
    $this->colorField = $colorfield;
  }

  /**
   * Getter for the day link.
   *
   * @return string
   *   The day link setting.
   */
  public function getDayLink() {
    return $this->dayLink;
  }

  /**
   * Setter for the day link.
   *
   * @param string $dayLink
   *   The day link setting.
   */
  public function setDayLink($dayLink) {
    $this->dayLink = $dayLink;
  }

  /**
   * Getter for the granularity.
   *
   * @return string
   *   The granularity property.
   */
  public function getGranularity() {
    return $this->granularity;
  }

  /**
   * Setter for the granularity.
   *
   * @param string $granularity
   *   The granularity property.
   */
  public function setGranularity($granularity) {
    $this->granularity = $granularity;
  }

  /**
   * Getter for the max items variable.
   *
   * @return int
   *   The maximum amount of items to show.
   */
  public function getMaxItems() {
    return $this->maxItems;
  }

  /**
   * Setter for the max items variable.
   *
   * @param int $maxItems
   *   The maximum amount of items to show.
   */
  public function setMaxItems($maxItems) {
    $this->maxItems = $maxItems;
  }

  /**
   * Getter for the max items style.
   *
   * @return string
   *   The maximum items style.
   */
  public function getMaxItemsStyle() {
    return $this->maxItemsStyle;
  }

  /**
   * Setter for the maximum items style.
   *
   * @param string $maxItemsStyle
   *   The maximum items style.
   */
  public function setMaxItemsStyle($maxItemsStyle) {
    $this->maxItemsStyle = $maxItemsStyle;
  }

  /**
   * Getter for the mini format variable.
   *
   * @return bool
   *   TRUE if the calendar is shown in mini, FALSE otherwise.
   */
  public function getMini() {
    return $this->mini;
  }

  /**
   * Setter for the mini format variable.
   *
   * @param bool $mini
   *   TRUE if the calendar is shown in mini, FALSE otherwise.
   */
  public function setMini($mini) {
    $this->mini = $mini;
  }

  /**
   * Getter for The multi- & allday style setting.
   *
   * @return string
   *   The multi- & allday style setting.
   */
  public function getMultiAllDayStyle() {
    return $this->multiAllDayStyle;
  }

  /**
   * Setter for The multi- & allday style setting.
   *
   * @param string $multialldaystyle
   *   The multi- & allday style setting.
   */
  public function setMultiAllDayStyle($multialldaystyle) {
    $this->multiAllDayStyle = $multialldaystyle;
  }

  /**
   * Getter for the multiday theme.
   *
   * @return int
   *   The index number of the multiday theme.
   */
  public function getMultiDayTheme() {
    return $this->multiDayTheme;
  }

  /**
   * Setter for the multi day theme variable.
   *
   * @param int $multiDayTheme
   *   The index number of the multiday theme.
   */
  public function setMultiDayTheme($multiDayTheme) {
    $this->multiDayTheme = $multiDayTheme;
  }

  /**
   * Getter for The multi- & allday style setting.
   *
   * @return string
   *   The multi- & allday title setting.
   */
  public function getMultiAllDayTitle() {
    return $this->multiAllDayTitle;
  }

  /**
   * Setter for The multi- & allday title setting.
   *
   * @param string $multialldaytitle
   *   The multi- & allday title setting.
   */
  public function setMultiAllDayTitle($multialldaytitle) {
    $this->multiAllDayTitle = $multialldaytitle;
  }

  /**
   * Getter for The multi- & allday view mode.
   *
   * @return string
   *   The multi- & allday view mode.
   */
  public function getmultiAllDayViewMode()
  {
    return $this->multiAllDayViewMode;
  }

  /**
   * Setter for The multi- & allday view mode.
   *
   * @param string $multialldayviewmode
   *   The multi- & allday view mode.
   */
  public function setmultiAllDayViewMode($multialldayviewmode) {
    $this->multiAllDayViewMode = $multialldayviewmode;
  }

  /**
   * Getter for the name size.
   *
   * @return int
   *   The name size.
   */
  public function getDayNameSize() {
    return $this->dayNameSize;
  }

  /**
   * Setter for the name size.
   *
   * @param int $nameSize
   *   The name size.
   */
  public function setDayNameSize($nameSize) {
    $this->dayNameSize = $nameSize;
  }

  /**
   * Getter for the show week numbers variable.
   *
   * @return bool
   *   TRUE to display the week numbers, FAlSE otherwise.
   */
  public function getShowWeekNumbers() {
    return $this->showWeekNumbers;
  }

  /**
   * Setter for the show week numbers variable.
   *
   * @param bool $showWeekNumbers
   *   TRUE to display the week numbers, FAlSE otherwise.
   */
  public function setShowWeekNumbers($showWeekNumbers) {
    $this->showWeekNumbers = $showWeekNumbers;
  }
  /**
   * Getter for the theme style variable.
   *
   * @return int
   *   The index number of the theme style.
   */
  public function getThemeStyle() {
    return $this->themeStyle;
  }

  /**
   * Setter for the theme style variable.
   *
   * @param int $themeStyle
   *   The index number of the theme style.
   */
  public function setThemeStyle($themeStyle) {
    $this->themeStyle = $themeStyle;
  }

  /**
   * Getter for the week link.
   *
   * @return string
   *   The week link setting.
   */
  public function getWeekLink() {
    return $this->weekLink;
  }

  /**
   * Setter for the week link.
   *
   * @param string $weekLink
   *   The week link setting.
   */
  public function setWeekLink($weekLink) {
    $this->weekLink = $weekLink;
  }

}
