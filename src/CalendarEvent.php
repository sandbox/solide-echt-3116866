<?php

namespace Drupal\calendar;

/**
 * Defines a calendar event object.
 */
class CalendarEvent {

  /**
   * Startdate for the event.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   *   The start date for the event.
   */
  protected $startDate;

  /**
   * Enddate for the event.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   *   The end date for the event.
   */
  protected $endDate;

  /**
   * The rendered row.
   *
   * @var array
   *   The rendered row.
   */
  protected $row;

  /**
   * The granularity for the view display.
   *
   * @var string
   *   The granularity of this event (e.g. "day", "second").
   */
  protected $granularity;

  /**
   * Allday events are defined as having starttime 00:00 and endtime 23:59.
   *
   * @var bool
   *   Defines whether or not this event's duration is all day.
   */
  protected $allDay;

  /**
   * The timezone for the event as defined by its author.
   *
   * This needs a core patch to set this per entity.
   *
   * @var \DateTimeZone
   *   The timezone of the event.
   */
  protected $timezone;

  /**
   * Stripelabels not used (yet).
   *
   * @var array
   *   The array of labels to be used for this stripe option.
   */
  protected $stripeLabels;

  /**
   * Stripelabels not used (yet).
   *
   * @var string
   *  The hex code array of the color to be used.
   */
  protected $stripeHexes;

  /**
   * Multiday events are defined as having startdate different from enddate.
   *
   * It takes timezone into account.
   *
   * @var bool
   *  Whether this event covers multiple days.
   */
  protected $multiDay;

  /**
   * Entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * Label.
   *
   * @var string
   */
  protected $label;

  /**
   * Offset (necessary?).
   *
   * @var \DateInterval
   */
  protected $offSet;

  /**
   * Length (necessary?).
   *
   * @var \DateInterval
   */
  protected $length;

  /**
   * Event id.
   *
   * @var int
   */
  protected $id;

  /**
   * Event weekno.
   *
   * @var int
   */
  protected $weekno;

  /**
   * Event color.
   *
   * @var string
   */
  protected $color;

  /**
   * Getter for weekno.
   *
   * @return int
   *   The getter to fetch the weekno.
   */
  public function getWeekno() {
    return $this->weekno;
  }

  /**
   * Setter for the weekno.
   *
   * @param int $weekno
   *   The setter for the weekno.
   */
  public function setWeekno(int $weekno) {
    $this->weekno = $weekno;
  }

  /**
   * Set a row as array with values (caldays).
   *
   * @param array $row
   *   The setter to attach the rendered row.
   */
  public function setRow(array $row) {
    $this->row = $row;
  }

  /**
   * Get a row as array with values (caldays).
   *
   * @return array
   *   The getter to fetch the attached rendered row.
   */
  public function getRow() {
    return $this->row;
  }

  /**
   * Getter for the event id.
   *
   * @return int
   *   The id for the event
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Setter for the event id.
   *
   * @@param int $id
   *   The event id is derived from related entity.
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * Getter for the entity type id.
   *
   * @todo Remove for getType.
   *
   * @return string
   *   The entity type id.
   */
  public function getEntityTypeId() {
    return $this->entity->getEntityTypeId();
  }

  /**
   * Get the complete entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Tthe event entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Getter for the type.
   *
   * @return string
   *   The type of the entity.
   */
  public function getType() {
    return $this->entity->getEntityTypeId();
  }

  /**
   * Getter the entity bundle.
   *
   * @return string
   *   The entity bundle.
   */
  public function getBundle() {
    return $this->entity->bundle();
  }

  /**
   * Getter for color.
   *
   * @return string
   *   The color of the entity.
   */
  public function getColor() {
    return $this->color;
  }

  /**
   * Getter for the start date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The start date.
   */
  public function getStartDate() {
    return $this->startDate;
  }

  /**
   * Setter for the start date.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $startDate
   *   The start date.
   */
  public function setStartDate($startDate) {
    $this->startDate = $startDate;
  }

  /**
   * Getter for the offset.
   *
   * @return \DateInterval
   *   The date offset.
   */
  public function getOffset() {
    return $this->offSet;
  }

  /**
   * Setter for the offset.
   *
   * @param \DateInterval $offSet
   *   The offset date.
   */
  public function setOffSet($offSet) {
    $this->offSet = $offSet;
  }

  /**
   * Getter for the length.
   *
   * @return \DateInterval
   *   The event length.
   */
  public function getLength() {
    return $this->length;
  }

  /**
   * Setter for the length.
   *
   * @param \DateInterval $length;
   *   The event length.
   */
  public function setLength($length) {
    $this->length = $length;
  }

  /**
   * Getter for the end date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The end date.
   */
  public function getEndDate() {
    return $this->endDate;
  }

  /**
   * Setter for the end date.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $endDate
   *   The end date.
   */
  public function setEndDate($endDate) {
    $this->endDate = $endDate;
  }

  /**
   * Getter for the event granularity.
   *
   * @return string
   *   The event granularity.
   */
  public function getGranularity() {
    return $this->granularity;
  }

  /**
   * Setter for the event granularity.
   *
   * @param string $granularity
   *   The event granularity.
   */
  public function setGranularity($granularity) {
    $this->granularity = $granularity;
  }

  /**
   * Getter for the all day property.
   *
   * @return boolean
   *   TRUE if the event is all day, FALSE otherwise.
   */
  public function getAllDay() {
    return $this->allDay;
  }

  /**
   * Setter for the all day property.
   *
   * @param bool $allDay
   *   TRUE if the event is all day, FALSE otherwise.
   */
  public function setAllDay($allDay) {
    $this->allDay = $allDay;
  }

  /**
   * The getter which indicates whether an event covers multiple days.
   *
   * @return bool
   *   Bool TRUE if is multiday.
   */
  public function getMultiDay() {
    return $this->multiDay;
  }

  /**
   * The setter to indicate whether an event covers multiple days.
   *
   * @param bool $multiDay
   *   TRUE if multiday.
   */
  public function setMultiDay($multiDay) {
    $this->multiDay = $multiDay;
  }

  /**
   * Getter for the timezone property.
   *
   * @return \DateTimeZone
   *   The timezone of this event.
   */
  public function getTimezone() {
    return $this->timezone;
  }

  /**
   * Setter for the timezone property.
   *
   * @param \DateTimeZone $timezone
   *   The timezone of this event.
   */
  public function setTimezone($timezone) {
    $this->timezone = $timezone;
  }

  /**
   * The title setter.
   *
   * @param string $label
   *   The title of the event.
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * The color setter.
   *
   * @@param string $color
   *   The title of the event.
   */
  public function setColor($color) {
    $this->color = $color;
  }

  /**
   * The title getter.
   *
   * @return string
   *   The title of the event.
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Getter for the url.
   *
   * @return string
   *   The public url to this event.
   */
  public function getUrl() {
    return $this->getUrl();
  }

  /**
   * Getter for the stripe label array.
   *
   * If no array is defined, this initializes the variable to an empty array.
   *
   * @return array
   *   The stripe labels.
   */
  public function getStripeLabels() {
    if (!isset($this->stripeLabels)) {
      $this->stripeLabels = [];
    }
    return $this->stripeLabels;
  }

  /**
   * Setter for the stripe label array.
   *
   * @param string $stripeLabels
   *   The stripe labels.
   */
  public function setStripeLabels($stripeLabels) {
    $this->stripeLabels = $stripeLabels;
  }

  /**
   * Getter for the stripe hex code array.
   *
   * If no array is defined, this initializes the variable to an empty array.
   *
   * @return array
   *   The stripe hex code array.
   */
  public function getStripeHexes() {
    if (!isset($this->stripeHexes)) {
      $this->stripeHexes = [];
    }
    return $this->stripeHexes;
  }

  /**
   * The setter for the stripe hex code array.
   *
   * @param string $stripeHexes
   *   The stripe hex code array.
   */
  public function setStripeHexes($stripeHexes) {
    $this->stripeHexes = $stripeHexes;
  }

  /**
   * Add a single strip hex.
   *
   * @param string $stripeHex
   */
  public function addStripeHex($stripeHex) {
    $this->stripeHexes[] = $stripeHex;
  }

  /**
   * Add a single strip label.
   *
   * @param string $stripeLabel
   */
  public function addStripeLabel($stripeLabel) {
    $this->stripeLabels[] = $stripeLabel;
  }

}
